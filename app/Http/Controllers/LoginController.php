<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\App;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $user = DB::table('users')->where('user', $request->email)->value('user');
        $password = DB::table('users')->where('user', $request->email)->value('password');
        $password = Hash::check($request->password, $password);
        $state = DB::table('personal')->where('email', $request->email)->value('state_id');

        if ($user && $password && ($state === 1)) {
            $code = DB::table('personal')->where('email', $request->email)->value('code');
            session(['code' => $code]);
            App::record(DB::table('personal')->where('code', $code)->value('id'), null, 1, 10, 'Login');
        }
        return redirect()->route('principal');
    }

    public function logout()
    {
        if (!session('code'))
            return redirect()->route('principal');
        App::record(DB::table('personal')->where('code', session('code'))->value('id'), null, 1, 11, 'Logout');
        session()->flush();
        return redirect()->route('principal');
    }
}
