<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Personal;
use App\Models\Department;
use App\Models\Gender;
use App\Models\University;
use App\Models\Career;
use App\Models\Speciality;
use App\Models\AcademicDegree;
use App\Models\PersonalInformation;
use App\Models\Fee;
use App\App;

class PersonalAccountController extends Controller
{
    public function personalAccount($code) {
        $id =  DB::table('personal')->where('code', $code)->value('id');
        $personal = DB::table('personal')->where('code', $code)->get();
        $personal = $personal[0];

        $personalinformation_id = DB::table('personalinformation')->where('personal_id', $personal->id)->value('id');

        if ($personalinformation_id) {
            $personalinformation = DB::table('personalinformation')->where('personal_id', $personal->id)->get();
            $personalinformation = $personalinformation[0];
        } else {
            $personalinformation =  null;
        }

        return view('personalaccount.personalaccount', [
            'user' => DB::table('personal')->where('code', session('code'))->value('names'),
            'group' => DB::table('personal')->where('code', session('code'))->value('group_id'),
            'permission' => DB::table('personal')->where('code', session('code'))->value('permission_id'),
            'code' =>  $code,
            'personal' => $personal,
            'personalinformation' => $personalinformation,
            'department' => Department::where('state_id', 1)->orWhere('state_id', 2)->get(),
            'gender' => Gender::all(),
            'university' => University::where('state_id', 1)->orWhere('state_id', 2)->get(),
            'career' => Career::where('state_id', 1)->orWhere('state_id', 2)->get(),
            'speciality' => Speciality::where('state_id', 1)->orWhere('state_id', 2)->get(),
            'academicdegree' => AcademicDegree::where('state_id', 1)->orWhere('state_id', 2)->get(),
            'fee' => Fee::where('personal_id', $id)->get(),
        ]);
    }

    public function personalInformationCreate(Request $request) {
        $id = DB::table('personal')->where('code', $request->code)->value('id');
        $code = App::Code();
        $perinf = new PersonalInformation;
        $perinf->code = $code;
        $perinf->personal_id = $id;
        $perinf->ci = $request->ci;
        $perinf->department_id = $request->department;
        $perinf->gender_id = $request->gender;
        $perinf->cellular = $request->cellular;
        $perinf->telephone = $request->telephone;
        $perinf->address = $request->address;
        $perinf->university_id = $request->university;
        $perinf->career_id = $request->career;
        $perinf->speciality_id = $request->speciality;
        $perinf->academicdegree_id = $request->academicdegree;
        $perinf->ru = $request->ru;
        $perinf->description = $request->description;
        $perinf->creationdate = App::DateTime();
        $perinf->save();

        $per = Personal::find($id);
        $per->group_id = 3;
        $per->save();

        for ($i=1; $i <= (30*12); $i++) {
            $fee = new Fee;
            $fee->fee = $i;
            $fee->state_id = 1;
            $fee->personal_id = $id;
            $fee->creationdate = App::DateTime();
            $fee->save();
        }

        return redirect()->route('personalAccount', [
            'code' => DB::table('personal')->where('code', $request->code)->value('code')
        ]);
    }

    public function fee($code, $id, $fee) {
        $id = DB::table('fee')->where('id', $id)->where('fee', $fee)->value('id');
        $fee = Fee::find($id);
        $fee->fee5 = 1;
        $fee->upgradedate = App::DateTime();
        $fee->save();

        return redirect()->route('personalAccount', [
            'code' => $code
        ]);
    }

    public function nofee($code, $id, $fee) {
        $id = DB::table('fee')->where('id', $id)->where('fee', $fee)->value('id');
        $fee = Fee::find($id);
        $fee->fee5 = null;
        $fee->upgradedate = App::DateTime();
        $fee->save();

        return redirect()->route('personalAccount', [
            'code' => $code
        ]);
    }
}
