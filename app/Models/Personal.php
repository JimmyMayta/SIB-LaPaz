<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    use HasFactory;
    protected $table = 'personal';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'code',
        'names',
        'firstlastname',
        'secondlastname',
        'email',
        'user_id',
        'group_id',
        'permission_id',
        'state_id',
        'personal_id',
        'description',
        'detail',
        'creationdate',
        'upgradedate',
        'eliminationdate'
    ];
}
