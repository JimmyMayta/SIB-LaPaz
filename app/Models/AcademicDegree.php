<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AcademicDegree extends Model
{
    use HasFactory;
    protected $table = 'academicdegree';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'academicdegree',
        'abbreviation',
        'state_id',
        'personal_id',
        'description',
        'detail',
        'creationdate',
        'upgradedate',
        'eliminationdate'
    ];
}
