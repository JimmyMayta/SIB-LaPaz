<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    use HasFactory;
    protected $table = 'universities';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'university',
        'state_id',
        'personal_id',
        'description',
        'detail',
        'creationdate',
        'upgradedate',
        'eliminationdate'
    ];
}
