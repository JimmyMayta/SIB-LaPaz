<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    protected $table = 'departments';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'department',
        'abbreviation',
        'state_id',
        'personal_id',
        'description',
        'detail',
        'creationdate',
        'upgradedate',
        'eliminationdate'
    ];
}
