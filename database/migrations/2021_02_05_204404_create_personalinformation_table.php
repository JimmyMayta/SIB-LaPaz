<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreatePersonalinformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personalinformation', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->integer('id')->autoIncrement();
            $table->string('code', 100);
            $table->integer('personal_id');
            $table->string('ci', 100);
            $table->integer('department_id');
            $table->integer('gender_id');
            $table->string('cellular', 100)->nullable();
            $table->string('telephone', 100)->nullable();
            $table->string('address', 100)->nullable();
            $table->integer('university_id');
            $table->integer('career_id');
            $table->integer('speciality_id');
            $table->integer('academicdegree_id');
            $table->string('ru', 100)->nullable();
            $table->string('description', 300)->nullable();
            $table->string('detail', 300)->nullable();
            $table->dateTime('creationdate')->nullable();
            $table->dateTime('upgradedate')->nullable();
            $table->dateTime('eliminationdate')->nullable();
            $table->foreign('personal_id')->references('id')->on('personal');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('gender_id')->references('id')->on('genders');
            $table->foreign('university_id')->references('id')->on('universities');
            $table->foreign('career_id')->references('id')->on('careers');
            $table->foreign('speciality_id')->references('id')->on('specialities');
            $table->foreign('academicdegree_id')->references('id')->on('academicdegree');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personalinformation');
    }
}
