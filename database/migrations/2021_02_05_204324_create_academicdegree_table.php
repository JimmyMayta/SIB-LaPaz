<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateAcademicdegreeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academicdegree', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->integer('id')->autoIncrement();
            $table->string('academicdegree', 100);
            $table->string('abbreviation', 100);
            $table->integer('state_id');
            $table->integer('personal_id');
            $table->string('description', 300)->nullable();
            $table->string('detail', 300)->nullable();
            $table->dateTime('creationdate')->nullable();
            $table->dateTime('upgradedate')->nullable();
            $table->dateTime('eliminationdate')->nullable();
            $table->foreign('state_id')->references('id')->on('state');
            $table->foreign('personal_id')->references('id')->on('personal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academicdegree');
    }
}
