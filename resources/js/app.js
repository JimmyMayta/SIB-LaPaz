require('./bootstrap');

var password1 = document.getElementById('password1');
var password2 = document.getElementById('password2');
function validatePassword() {
  if (password1.value !== password2.value) {
    password2.setCustomValidity('La contraseña no es igual');
  } else {
    password2.setCustomValidity('');
  }
}
password1.onchange = validatePassword;
password2.onkeyup = validatePassword;


$(document).ready(function(){
  $('#TableDataTables').DataTable({
    language: {
      url: 'language/es_es.json'
    }
  });
});

var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})

// document.getElementById("TextareaDescription").placeholder = "Descripción";
