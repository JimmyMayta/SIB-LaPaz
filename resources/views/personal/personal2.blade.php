<div class="modal fade" id="personal2Modal{{ $per->id }}" tabindex="-1" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">{{ $per->names }} {{ $per->firstlastname }} {{ $per->secondlastname }}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('personalDos') }}" method="post">
          @csrf
          <input type="hidden" name="personal" value="{{ $per->id }}">
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Grupo</label>
            <div class="col-sm-10">
              <select class="form-select" aria-label="Default select" name="group">
                <option value="1" @if ($per->group_id === 1) selected @endif>Administrador</option>
                <option value="2" @if ($per->group_id === 2) selected @endif>Personal</option>
                <option value="3" @if ($per->group_id === 3) selected @endif>Asociado</option>
                <option value="4" @if ($per->group_id === 4) selected @endif>Curioso</option>
              </select>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Permiso</label>
            <div class="col-sm-10">
              <select class="form-select" aria-label="Default select" name="permission">
                <option value="1" @if ($per->permission_id === 1) selected @endif>Lectura</option>
                <option value="2" @if ($per->permission_id === 2) selected @endif>Escritura</option>
              </select>
            </div>
          </div>

          <button type="submit" class="btn btn-primary btn-sm">Guardar</button>
        </form>
      </div>
    </div>
  </div>
</div>
