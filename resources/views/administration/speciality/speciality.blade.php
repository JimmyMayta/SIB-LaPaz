<div class="accordion-item">
  <h2 class="accordion-header" id="headingTwo">
    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
    data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
      Especialidad
    </button>
  </h2>
  <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
    data-bs-parent="#accordionExample">
    <div class="accordion-body">
      <div class="accordion-body">
        @if ($permission == 2)
        <button type="button" class="btn btn-success btn-sm mb-1"
        data-bs-toggle="modal" data-bs-target="#specialityModal">
          <i class="bi bi-plus" style="font-size: 1rem; color: rgb(255, 255, 255);"></i>
        </button>
        @endif

        <div class="table-responsive">
          <table class="table align-middle table-sm">
            <thead>
              <tr>
                <td>N.</td>
                <td>Grado academico</td>
                <td>Descripción</td>
                @if ($permission == 2)
                  <td></td>
                  <td></td>
                @endif
              </tr>
            </thead>
            <tbody>
              @foreach ($speciality as $spe)
                <tr>
                  <td>{{ $spe->id }}</td>
                  <td>{{ $spe->speciality }}</td>
                  <td>{{ $spe->description }}</td>
                  @if ($permission == 2)
                  <td>
                    <a href="" class="text-dark" data-bs-toggle="modal" data-bs-target="#specialityUpdateModal{{ $spe->id }}">
                      <i class="bi bi-pencil-fill" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar"></i>
                    </a>
                  </td>
                  <td>
                    <a href="{{ route('specialityDelete', $spe->id) }}" class="text-dark" data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar">
                      <i class="bi bi-trash-fill"></i>
                    </a>
                  </td>
                  @endif
                </tr>
                @include('administration.speciality.specialityupdate')
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('administration.speciality.specialitycreate')
