<div class="accordion-item">
  <h2 class="accordion-header" id="headingSix">
    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
    data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
      Areas SIB - La Paz
    </button>
  </h2>
  <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix"
  data-bs-parent="#accordionExample">
    <div class="accordion-body">
      @if ($permission == 2)
        <button type="button" class="btn btn-success btn-sm mb-1"
          data-bs-toggle="modal" data-bs-target="#areaModal">
          <i class="bi bi-plus" style="font-size: 1rem; color: rgb(255, 255, 255);"></i>
        </button>
      @endif

      <div class="table-responsive">
        <table class="table align-middle table-sm">
          <thead>
            <tr>
              <td>N.</td>
              <td>Carera</td>
              <td>Descripción</td>
              @if ($permission == 2)
                <td></td>
                <td></td>
              @endif
            </tr>
          </thead>
          <tbody>
            @foreach ($area as $are)
              <tr>
                <td>{{ $are->id }}</td>
                <td>{{ $are->area }}</td>
                <td>{{ $are->description }}</td>
                @if ($permission == 2)
                  <td>
                    <a href="" class="text-dark" data-bs-toggle="modal" data-bs-target="#areaUpdateModal{{ $are->id }}">
                      <i class="bi bi-pencil-fill" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar"></i>
                    </a>
                  </td>
                  <td>
                    <a href="{{ route('areaDelete', $are->id) }}" class="text-dark" data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar">
                      <i class="bi bi-trash-fill"></i>
                    </a>
                  </td>
                @endif
              </tr>
              @include('administration.area.areaupdate')
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@include('administration.area.areacreate')
