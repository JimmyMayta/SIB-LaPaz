<div class="accordion-item">
  <h2 class="accordion-header" id="headingThree">
    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
    data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
      Carrera
    </button>
  </h2>
  <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
  data-bs-parent="#accordionExample">
    <div class="accordion-body">
      @if ($permission == 2)
        <button type="button" class="btn btn-success btn-sm mb-1"
          data-bs-toggle="modal" data-bs-target="#careerModal">
          <i class="bi bi-plus" style="font-size: 1rem; color: rgb(255, 255, 255);"></i>
        </button>
      @endif

      <div class="table-responsive">
        <table class="table align-middle table-sm">
          <thead>
            <tr>
              <td>N.</td>
              <td>Carera</td>
              <td>Descripción</td>
              @if ($permission == 2)
                <td></td>
                <td></td>
              @endif
            </tr>
          </thead>
          <tbody>
            @foreach ($career as $car)
              <tr>
                <td>{{ $car->id }}</td>
                <td>{{ $car->career }}</td>
                <td>{{ $car->description }}</td>
                @if ($permission == 2)
                  <td>
                    <a href="" class="text-dark" data-bs-toggle="modal" data-bs-target="#careerUpdateModal{{ $car->id }}">
                      <i class="bi bi-pencil-fill" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar"></i>
                    </a>
                  </td>
                  <td>
                    <a href="{{ route('careerDelete', $car->id) }}" class="text-dark" data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar">
                      <i class="bi bi-trash-fill"></i>
                    </a>
                  </td>
                @endif
              </tr>
              @include('administration.career.careerupdate')
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('administration.career.careercreate')
