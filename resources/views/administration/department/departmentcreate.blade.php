<div class="modal fade" id="departmentModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Grado academico</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('departmentCreate') }}" method="post">
          @csrf
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="department" placeholder="Departamento">
          </div>

          <div class="input-group mb-3">
            <input type="text" class="form-control" name="abbreviation" placeholder="Abreviatura">
          </div>

          <div class="mb-3">
            <textarea id="TextareaDescription" class="form-control" name="description" placeholder="Descripción"></textarea>
          </div>

          <button type="submit" class="btn btn-primary btn-sm" >Guardar</button>
        </form>
      </div>
    </div>
  </div>
</div>
