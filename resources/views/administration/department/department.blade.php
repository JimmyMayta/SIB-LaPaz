<div class="accordion-item">
  <h2 class="accordion-header" id="headingFive">
    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
    data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
      Departamento
    </button>
  </h2>
  <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive"
  data-bs-parent="#accordionExample">
    <div class="accordion-body">
      @if ($permission == 2)
        <button type="button" class="btn btn-success btn-sm mb-1"
          data-bs-toggle="modal" data-bs-target="#departmentModal">
          <i class="bi bi-plus" style="font-size: 1rem; color: rgb(255, 255, 255);"></i>
        </button>
      @endif
      <div class="table-responsive">
        <table class="table align-middle table-sm">
          <thead>
            <tr>
              <td>N.</td>
              <td>Departamento</td>
              <td>Abreviatura</td>
              <td>Descripción</td>
              @if ($permission == 2)
                <td></td>
                <td></td>
              @endif
            </tr>
          </thead>
          <tbody>
            @foreach ($department as $dep)
              <tr>
                <td>{{ $dep->id }}</td>
                <td>{{ $dep->department }}</td>
                <td>{{ $dep->abbreviation }}</td>
                <td>{{ $dep->description }}</td>
                @if ($permission == 2)
                  <td>
                    <a href="" class="text-dark" data-bs-toggle="modal" data-bs-target="#departmentUpdateModal{{ $dep->id }}">
                      <i class="bi bi-pencil-fill" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar"></i>
                    </a>
                  </td>
                  <td>
                    <a href="{{ route('departmentDelete', $dep->id) }}" class="text-dark" data-bs-toggle="tooltip"
                      data-bs-placement="top" title="Eliminar">
                      <i class="bi bi-trash-fill"></i>
                    </a>
                  </td>
                @endif
              </tr>
              @include('administration.department.departmentupdate')
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@include('administration.department.departmentcreate')
