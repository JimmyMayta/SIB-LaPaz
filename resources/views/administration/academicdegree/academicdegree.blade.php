<div class="accordion-item">
  <h2 class="accordion-header" id="headingOne">
    <button class="accordion-button" type="button" data-bs-toggle="collapse"
    data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
      Grado academico
    </button>
  </h2>
  <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
    data-bs-parent="#accordionExample">
    <div class="accordion-body">
      @if ($permission == 2)
        <button type="button" class="btn btn-success btn-sm mb-1"
          data-bs-toggle="modal" data-bs-target="#academicDegreeModal">
          <i class="bi bi-plus" style="font-size: 1rem; color: rgb(255, 255, 255);"></i>
        </button>
      @endif

      <div class="table-responsive">
        <table class="table align-middle table-sm">
          <thead>
            <tr>
              <td>N.</td>
              <td>Grado academico</td>
              <td>Abrebiatura</td>
              <td>Descripción</td>
              @if ($permission == 2)
                <td></td>
                <td></td>
              @endif
            </tr>
          </thead>
          <tbody>
            @foreach ($academicdegree as $ad)
              <tr>
                <td>{{ $ad->id }}</td>
                <td>{{ $ad->academicdegree }}</td>
                <td>{{ $ad->abbreviation }}</td>
                <td>{{ $ad->description }}</td>
                @if ($permission == 2)
                  <td>
                    <a href="" class="text-dark" data-bs-toggle="modal" data-bs-target="#academicDegreeUpdateModal{{ $ad->id }}">
                      <i class="bi bi-pencil-fill" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar"></i>
                    </a>
                  </td>
                  <td>
                    <a href="{{ route('academicDegreeDelete', $ad->id) }}" class="text-dark" data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar">
                      <i class="bi bi-trash-fill"></i>
                    </a>
                  </td>
                @endif
              </tr>
              @include('administration.academicdegree.academicdegreeupdate')
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('administration.academicdegree.academicdegreecreate')
