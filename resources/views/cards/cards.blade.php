<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-4 mb-3">
  @for ($i = 0; $i < 30; $i++)
    <div class="col">
      <div class="card h-100">
        <img src="{{ asset('images/20210203080401.jpg') }}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
        </div>
        <div class="card-footer">
          <small class="text-muted">Last updated {{ $i }} mins ago</small>
        </div>
      </div>
    </div>
  @endfor
</div>
